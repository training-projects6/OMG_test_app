//
//  OMG_test_appApp.swift
//  OMG_test_app
//
//  Created by Artem Soloviev on 29.02.2024.
//

import SwiftUI

@main
struct OMG_test_appApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
