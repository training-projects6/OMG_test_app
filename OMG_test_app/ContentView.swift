//
//  ContentView.swift
//  OMG_test_app
//
//  Created by Artem Soloviev on 29.02.2024.
//

import SwiftUI


struct Cell: Hashable, Identifiable {
    
    var id: Int
    var number: Int
}

struct CellView: View {
    
    @State private var isTapped = false
    var cell: Cell
    
    var body: some View {
        VStack {
            Button {
                isTapped = false
            } label: {
                Text("\(cell.number)")
                    .padding()
                    .frame(width: 80, height: 80)
                    .background(RoundedRectangle(cornerRadius: 20)
                        .strokeBorder(.secondary, lineWidth: 0.5))
                    .scaleEffect(isTapped ? 0.8 : 1.0)
                
            }
            .buttonStyle(.plain)
            .simultaneousGesture(
                LongPressGesture(minimumDuration: 0.1)
                    .onEnded { _ in
                        withAnimation {
                            isTapped = true
                        }
                    }
            )
        }
    }
}


struct ContentView: View {
    
    @State private var  randomList = [[Cell]]()
    
    var body: some View {
        
        NavigationStack {
            
            ScrollView(.vertical, showsIndicators: false) {
                
                LazyVStack {
                    ForEach(randomList.indices, id: \.self) { index in
                        
                        ScrollView(.horizontal, showsIndicators: false) {
                            
                            HStack {
                                ForEach(randomList[index], id: \.self) { element in
                                    CellView(cell: element)
                                        .padding(.vertical)
                                }
                            }
                        }
                    }
                }
            }
            .padding()
            .navigationTitle("OMG")
        }
        .onAppear {
            generaterandomList()
            rundomNum()
        }
    }
    
    func rundomNum() {
        
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in
            for index in randomList.indices {
                guard let randomIndex = randomList[index].indices.randomElement() else { continue }
                let newValue = Int.random(in: 1...150)
                let oldCell = randomList[index][randomIndex]
                randomList[index][randomIndex] = Cell(id: oldCell.id, number: newValue)
            }
        }
    }
    
    func generaterandomList() {
        
        for r in 100...200 {
            var row = [Cell]()
            for c in 1...Int.random(in: 10...20) {
                let randomElement = Int.random(in: 1...150)
                row.append(Cell(id: r + c, number: randomElement))
            }
            randomList.append(row)
        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
